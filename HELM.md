# HELM

Helm is an open source package manager that automates the deployment of software for Kubernetes in a simple, consistent way. 

![img](images/helm/overview.png)

Ref: https://www.devopsschool.com/blog/a-quick-tutorial-of-helm/
