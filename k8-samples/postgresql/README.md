# PostgreSQL

https://gfw-blog.netlify.app/2020/01/11/postgres-on-kubernetes.html

Run below commands in the following order (set current directory for `postgresql`):

* docker volume create postgresdata
* kubectl create -f postgres-configmap.yaml
* kubectl create -f postgres-storage.yaml
* kubectl create -f postgres-deployment.yaml
* kubectl create -f postgres-service.yaml
* kubectl get svc postgres
  * For connecting PostgreSQL, we need to get the Node port from the service deployment.


## Connect from console

1. Connect to container
- kubectl exec -t -i postgres-yourappname-67b84fcfd5-8hr9s bash
- *to check if container is up, use: kubectl exec postgres-yourappname-67b84fcfd5-8hr9s ls
2. Log in
- psql -d db -U user
- *before check PostgreSQL details:
  - whereis postgresql
  - which psql
3. Executre example commands:
- SELECT VERSION();
4. In order to exit, type: \q
