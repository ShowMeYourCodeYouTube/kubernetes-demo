# Kubernetes

Kubernetes is an open-source container-orchestration system for automating application deployment, scaling, and management. It was originally designed by Google and is now maintained by the Cloud Native Computing Foundation.

With modern web services, users expect applications to be available 24/7, and developers expect to deploy new versions of those applications several times a day. Containerization helps package software to serve these goals, enabling applications to be released and updated in an easy and fast way without downtime. Kubernetes helps you make sure those containerized applications run where and when you want, and helps them find the resources and tools they need to work.

![img](./images/k8/overview.png)

![img](./images/k8/overview2.png)

References:
- **https://kubernetes.io/docs/tutorials/kubernetes-basics/**
- https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
- https://github.com/kubernetes/kubernetes/tree/master/CHANGELOG
- https://cloud.google.com/kubernetes-engine/docs/tutorials/persistent-disk#deploy_wordpress

---

In order to run k8s locally, you can use:

- Docker Desktop
  - License: Docker Desktop remains free for small businesses (fewer than 250 employees and less than $10 million in
    annual revenue), personal use, education, and non-commercial open-source projects.
- Rancher Desktop
  - Ref: https://github.com/rancher-sandbox/rancher-desktop/
  - Rancher Desktop is an open-source project that brings Kubernetes and container management to the desktop. It runs on
    Windows, macOS and Linux.
- minikube
  - It's recommended to use it for testing, especially on CI/CD.
  - *Remember to set up CPU and memory limits!*

## Details

Kubernetes coordinates a highly available cluster of computers that are connected to work as a single unit.

![img](./images/k8/img_2.png)

- The Master coordinates the cluster
- The Master is responsible for managing the cluster. The master coordinates all activities in your cluster, such as scheduling applications, maintaining applications' desired state, scaling applications, and rolling out new updates.
- Nodes are the workers that run applications
- A node is a VM or a physical computer that serves as a worker machine in a Kubernetes cluster. Each node has a Kubelet, which is an agent for managing the node and communicating with the Kubernetes master. The node should also have tools for handling container operations, such as Docker or rkt. A Kubernetes cluster that handles production traffic should have a minimum of three nodes.
- When you deploy applications on Kubernetes, you tell the master to start the application containers. The master schedules the containers to run on the cluster's nodes. The nodes communicate with the master using the Kubernetes API, which the master exposes. End users can also use the Kubernetes API directly to interact with the cluster.
- A Kubernetes cluster can be deployed on either physical or virtual machines. To get started with Kubernetes development, you can use Minikube.
- [etcd](https://etcd.io/) is a distributed, reliable key-value store for the most critical data of a distributed system. The etcd stores all cluster information, such as its current state, desired state, configuration of resources, and runtime data.
- The Kubernetes command-line tool, kubectl, allows you to run commands against Kubernetes clusters. You can use kubectl to deploy applications, inspect and manage cluster resources, and view logs. For more information including a complete list of kubectl operations, see the kubectl reference documentation.
  - Ref: https://kubernetes.io/docs/tasks/tools/#:~:text=The%20Kubernetes%20command%2Dline%20tool,see%20the%20kubectl%20reference%20documentation
- The kubelet is the primary "node agent" that runs on each node. It can register the node with the apiserver using one of: the hostname; a flag to override the hostname; or specific logic for a cloud provider. The kubelet works in terms of a PodSpec. A PodSpec is a YAML or JSON object that describes a pod.
  - Ref: https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/#:~:text=The%20kubelet%20is%20the%20primary,object%20that%20describes%20a%20pod
- Kube-proxy handles networking for Kubernetes services by managing IP tables or IPVS rules to route traffic to the appropriate pods.

![img](./images/k8/img_4.png)

### Secrets and ConfigMaps

In Kubernetes, both Secrets and ConfigMaps are used to store configuration data, but they serve different purposes and handle sensitive information in distinct ways:

Secrets:
- Used to store sensitive information like passwords, OAuth tokens, or SSH keys.
- Data is encoded (usually base64) and stored in Kubernetes, but Kubernetes does not provide encryption at rest by default (though encryption at rest can be configured).
- Typically, access to Secrets is more tightly controlled to ensure that only authorized entities (pods, users) can access them.

ConfigMap:
- Used to store non-sensitive configuration data, such as environment variables, configuration files, and command-line arguments.
- The data in ConfigMaps is stored in plain text, and there is no built-in encryption.
- ConfigMaps are more suitable for configurations that are not sensitive and do not require strict access controls.

In summary, Secrets are for sensitive data, while ConfigMaps are for non-sensitive configuration.

Both Secrets and ConfigMaps are stored in etcd in Kubernetes. etcd is the distributed key-value store used by Kubernetes to store all cluster data, including configurations, state, and metadata about the resources.

It's important to note that while both are stored in etcd, Secrets can be configured to use encryption at rest to ensure sensitive data is securely stored. ConfigMaps, however, don't have this security measure, and their data remains in plain text unless explicitly encrypted by the user or through additional layers of security.

## Container Runtime Interface (CRI)

The Container Runtime Interface (CRI) is the main protocol for the communication between the kubelet and Container Runtime. The Kubernetes Container Runtime Interface (CRI) defines the main gRPC protocol for the communication between the node components kubelet and container runtime.

Ref: https://kubernetes.io/docs/concepts/architecture/cri/

## Open Container Initiative (OCI)

The OCI is a Linux Foundation project that was created to establish industry standards for container images and runtime.

> The Open Container Initiative is an open governance structure for the express purpose of creating open industry standards around container formats and runtimes.

Reference: https://opencontainers.org/

## Objects In Kubernetes

See: [nginx.yml](k8-samples/nginx.yaml)

![img](images/k8/k8-components.png)

- **Deployment**
  - A Deployment provides declarative updates for Pods and ReplicaSets. 
  - You describe a desired state in a Deployment, and the Deployment Controller changes the actual state to the desired state at a controlled rate. You can define Deployments to create new ReplicaSets, or to remove existing Deployments and adopt all their resources with new Deployments.
  - https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
    - Liveness probe checkc whether a container app is responding to requests. If the liveness probe fails, the kubelet kills the Container, and the Container is subjected to its restart policy. If a Container does not provide a liveness probe, the default state is Success.
    - Readiness probes ensure that the container is healthy to serve incoming traffic. These probes can be used to delay sending traffic to a new instance until an application has completed tasks.
    - Startup probes detect when an application inside a container finishes its initialization. Kubernetes can identify slow-starting apps using the startup probe and not kill them due to the delay. If the readiness probe fails, the endpoint controller removes the Pod’s IP address from the endpoints of all Services that match the Pod.
    - References: 
      - https://4sysops.com/archives/kubernetes-health-checks-with-liveness-readiness-and-startup-probes
      - https://stackoverflow.com/questions/55423405/k8s-livenessprobe-vs-readinessprobe
- **Service**
  - A Service is a method for exposing a network application that is running as one or more Pods in your cluster.
  - A Kubernetes service is an abstraction that exposes a group of pods as a network service to an abstracted service name and IP address. Services provide discovery and routing between pods. The possible types are ClusterIP, NodePort, and LoadBalancer
  - https://kubernetes.io/docs/concepts/services-networking/service/
- **Ingress**
  - An API object that manages external access to the services in a cluster, typically HTTP. 
  - Ingress may provide load balancing, SSL termination and name-based virtual hosting.
  - https://kubernetes.io/docs/concepts/services-networking/ingress/
- **ClusterIP/NodePort/LoadBalancer**
  - ClusterIP: Services are reachable by pods/services in the Cluster
    - ClusterIP is assigned to a Service, not a Deployment.
    - Is the ClusterIP shared between services?
      - If you expose each Deployment with its own Service, Kubernetes assigns each Service a unique ClusterIP.
      - However, if both Deployments are behind the same Service, that Service will have a single ClusterIP, and traffic will be load-balanced between the pods of both Deployments.
  - NodePort: Services are reachable by clients on the same LAN/clients who can ping the K8s Host Nodes (and pods/services in the cluster) (Note for security your k8s host nodes should be on a private subnet, thus clients on the internet won't be able to reach this service)
  - LoadBalancer: Services are reachable by everyone connected to the internet* (Common architecture is L4 LB is publicly accessible on the internet by putting it in a DMZ or giving it both a private and public IP and k8s host nodes are on a private subnet)
  - Ref: https://stackoverflow.com/questions/41509439/whats-the-difference-between-clusterip-nodeport-and-loadbalancer-service-types
- **Replica Set**
  - A replica set is a group of instances that maintain the same data set.
  - ReplicaSet provides scaling by ensuring that a specified number of pod replicas are running at all times. However, for automated scaling based on resource usage, you need to use a Horizontal Pod Autoscaler (HPA) with the ReplicaSet.
- **Persistent Volume (PV) / Persistent Volume Claim (PVC)**
  - A persistent volume (PV) is the "physical" volume on the host machine that stores your persistent data. A
    persistent volume claim (PVC) is a request for the platform to create a PV for you, and you attach PVs to your pods
    via a PVC.
  - PVC is a declaration of need for storage that can at some point become available / satisfied - as in bound to some
    actual PV.
  - A PersistentVolume (PV) is a piece of storage in the cluster or central storage let's say 100GB. A
    PersistentVolumeClaim (PVC) is a request for storage by a user for the application to use 10GB.
     ```
     Pod -> PVC -> PV -> Host machine
     ```

References: 
- https://kubernetes.io/docs/concepts/overview/working-with-objects/
- https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/

### How does ClusterIp work?

- Service with ClusterIP:
  - When you create a Service in Kubernetes, it gets assigned a ClusterIP. This ClusterIP is a virtual IP that is reachable from inside the cluster.
  - The Service defines a selector, which is a label that matches a set of Pods. For example, a Service might target Pods with the label app=my-app.
- Traffic Routing:
  - When another Pod inside the cluster sends a request to the ClusterIP of the Service, Kubernetes automatically routes that request to one of the Pods that match the Service’s selector.
  - Kubernetes uses iptables or IPVS to create the internal routing rules. These rules ensure that traffic sent to the ClusterIP is forwarded to one of the Pods behind the Service.
- Proxying Traffic:
  - The kube-proxy component of Kubernetes is responsible for managing these internal routing rules.
  - When a request reaches the ClusterIP, kube-proxy ensures that the traffic is forwarded to one of the Pods in the set defined by the Service. This may be done through simple round-robin load balancing or other strategies based on configuration.
  
![img](images/k8s-service-clusterip.png)

- eth
  - These are Ethernet interfaces on the system (such as multiple physical or virtual NICs).
  - In Linux-based systems, it’s the default naming convention for the primary network interface. In a containerized environment (like Docker or Kubernetes), eth0 (the first Ethernet interface) is often the default network interface inside the container or pod.
  - It can be assigned an IP address that allows the system or container to communicate over the network, whether with the host or other machines.
- CNI (Container Network Interface):
  - CNI is a specification and set of libraries for configuring network interfaces in Linux containers.
  - It defines how containers (or Kubernetes pods) should be connected to a network.
  - A CNI plugin is used to manage the container's network setup (assigning IP addresses, setting up routes, etc.). Examples of CNI plugins include Calico, Flannel, and Weave.
  - CNI works with Kubernetes to provide networking to pods, enabling pod-to-pod communication, ingress/egress traffic, etc.
- veth
  - veth (Virtual Ethernet) is a pair of virtual network interfaces used for container networking.
  - A veth pair consists of two interfaces: one inside the container and the other attached to the host. This allows communication between the container and the host system (or other containers).
  - A veth0 interface inside a container connects to the veth1 interface on the host, creating a bridge for network traffic to flow between them.
- bridge/cni
  - Bridge refers to a network bridge in the context of containers or Kubernetes, which is used to connect multiple network interfaces together.
  - When using CNI plugins, the bridge is often a network mode or plugin type in Kubernetes. It creates a bridge between the pod's network interfaces and the node's network interfaces, allowing for communication between pods and the outside world.
  - The bridge plugin helps to connect containers to a common network and manage IP assignments and routing between containers on the same host.
- netfilter:
  - Netfilter is a framework in the Linux kernel for manipulating network traffic. It includes tools like iptables to configure firewalls, network address translation (NAT), and packet filtering rules.
  Netfilter helps control how traffic is handled between different network interfaces (e.g., eth0, veth, etc.).
  - In Kubernetes, Netfilter can be used for things like setting up Network Policies, controlling pod communication, or providing network security through firewall-like behavior.

[Refernece](https://medium.com/@rifewang/kubernetes-how-kube-proxy-and-cni-work-together-1255d273f291)

In summary:
  - The ClusterIP is the stable entry point for communication.
  - When you call the ClusterIP from inside the cluster, Kubernetes (via kube-proxy) automatically forwards the request to one of the underlying Pods that are part of the Service.
  - This allows you to access a set of Pods using a stable IP address (the ClusterIP), even though the individual Pods might be recreated or rescheduled.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: my-app
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080

```

Reference: https://devopscube.com/kubernetes-architecture-explained/

## Rolling Deployment vs Blue/Green Deployment vs Canary Deployment

Users expect applications to be available all the time and developers are expected to deploy new versions of them several times a day. In Kubernetes this is done with rolling updates. Rolling updates allow Deployments' update to take place with zero downtime by incrementally updating Pods instances with new ones. The new Pods will be scheduled on Nodes with available resources.

Rolling updates incrementally replace your resource's Pods with new ones, which are then scheduled on nodes with available resources. Rolling updates are designed to update your workloads without downtime.

![img](images/k8/rolling-deployment.gif)

---

![img](images/k8/blue-green-deployment.gif)

There are two environments, Blue environment which is "old" and contains one or more applications(instances or containers) and Green environment which is "new" and contains one or more applications(instances or containers).

Then, 100% traffic is quickly switched from Blue environment to Green environment.

---

![img](images/k8/canary-deployment.gif)

In addition, there is Canary Deployment which is gradual way of Blue-Green Deployment. In this case of Canary Deployment, 100% traffic is gradually switched from Blue environment to Green environment taking a longer time(30 minutes, hours, or days) than Blue-Green Deployment.

---

Reference: 
- https://stackoverflow.com/questions/42358118/blue-green-deployments-vs-rolling-deployments
- https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/

## How can we inject a configuration in Kubernetes?

- In Kubernetes, we have three constructs for injecting environment-based configuration:
  - Environment Variables
  - Volumes
  - ConfigMap

[Volume:](https://gist.github.com/matthewpalmer/bd10ea5360bdf3d735109a68050afeaa)
```text
kind: Pod
apiVersion: v1
metadata:
  name: simple-volume-pod
spec:
  # Volumes are declared by the pod. They share its lifecycle
  # and are communal across containers.
  volumes:
    # Volumes have a name and configuration based on the type of volume.
    # In this example, we use the emptyDir volume type
    - name: simple-vol
      emptyDir: {} # No extra configuration

  # Now, one of our containers can mount this volume and use it like
  # any other directory.
  containers:
    - name: my-container
      volumeMounts:
        - name: simple-vol # This is the name of the volume we set at the pod level
          mountPath: /var/simple # Where to mount this directory in our container
      
      # Now that we have a directory mounted at /var/simple, let's 
      # write to a file inside it!
      image: alpine
      command: ["/bin/sh"]
      args: ["-c", "while true; do date >> /var/simple/file.txt; sleep 5; done"]
```

[ConfigMap:](https://kubernetes.io/docs/concepts/storage/volumes/#configmap)
```text
apiVersion: v1
kind: Pod
metadata:
  name: configmap-pod
spec:
  containers:
    - name: test
      image: busybox:1.28
      command: ['sh', '-c', 'echo "The app is running!" && tail -f /dev/null']
      volumeMounts:
        - name: config-vol
          mountPath: /etc/config
  volumes:
    - name: config-vol
      configMap:
        name: log-config
        items:
          - key: log_level
            path: log_level
```

## Best practises

- Use namespaces
- Use readiness and liveness probes
- Use autoscaling
- Use resource requests and limits
- Deploy your pods as part of a Deployment, DaemonSet, ReplicaSet, or StatefulSet across nodes
- Use multiple nodes
  - Running K8s on a single node is not a good idea if you want to build in fault tolerance. Multiple nodes should be employed in your cluster so workloads can be spread between them.
- Use role-based access control (RBAC)
  - RBAC roles should be set up to grant using the principle of least privilege, i.e. only permissions that are required are granted. 
  - For example, the admin’s group may have access to all resources, and your operator’s group may be able to deploy but not be able to read Secrets.
  - RBAC is essential for managing secrets securely, as it enables you to control which users and components can create, read, update, or delete secrets. By implementing fine-grained access control, you can minimize the risk of unauthorized access and potential data breaches. Ref: https://blog.gitguardian.com/how-to-handle-secrets-in-kubernetes/

References:
- https://spacelift.io/blog/kubernetes-best-practices

## Alternatives to Kubernetes

Kubernetes alternatives, often based on Kubernetes' open source code, include cloud-based and third-party options:
- Amazon Elastic Kubernetes Service
- AWS Fargate
- Azure Container Instances
- Azure Kubernetes Service
- Docker Swarm
- Google Cloud Run
- Google Kubernetes Engine
- HashiCorp Nomad
- Linode Kubernetes Engine
- Mirantis Kubernetes Engine
- Rancher
- Red Hat OpenShift Container Platform
- Volcano

Ref: https://www.techtarget.com/searchitoperations/tip/Can-you-use-Kubernetes-without-Docker

## K8s tools

- [k9s](https://github.com/derailed/k9s)
  - K9s provides a terminal UI to interact with your Kubernetes clusters. 
  - The aim of this project is to make it easier to navigate, observe and manage your applications in the wild. 
  - K9s continually watches Kubernetes for changes and offers subsequent commands to interact with your observed resources.
- [kubectx + kubens](https://github.com/ahmetb/kubectx)
  - kubectx is a tool to switch between contexts (clusters) on kubectl faster.
  - kubens is a tool to switch between Kubernetes namespaces (and configure them for kubectl) easily.
- [krew](https://github.com/kubernetes-sigs/krew)
  - Krew is the package manager for kubectl plugins.
  - rew is an unofficial package that allows to install some k8S related package/plugins. Helm Charts allows deploying kubernetes resources with yaml templates.

## Service Mesh vs Kubernetes

- Kubernetes
  - What it is: A container orchestration platform that automates the deployment, scaling, and management of containerized applications.
  - Main Role: Manages infrastructure-level concerns such as scheduling, scaling, and networking of containers.
  - Key Features:
    - Pod scheduling & orchestration
    - Auto-scaling
    - Load balancing
    - Service discovery (via Kubernetes Services)
  - Basic networking and security policies
- Service Mesh
   - What it is: A dedicated infrastructure layer that manages service-to-service communication in a microservices architecture. Examples: Istio, Linkerd, Consul.
   - Main Role: Improves observability, security, and reliability of inter-service communication.
   - Key Features:
     - Service-to-Service Communication: Manages traffic between microservices.
     - Traffic Management: Controls traffic routing, load balancing, and retries.
     - Security: Provides encryption (TLS) and authentication (mTLS) between services.
     - Observability: Collects metrics, logs, and traces for monitoring and debugging.
     - Policy Enforcement: Implements access control, rate limiting, and circuit breaking.
- How They Work Together
  - Kubernetes runs and manages containers but does not provide advanced networking features beyond basic service discovery and load balancing.
  - A Service Mesh (like Istio or Linkerd) runs on top of Kubernetes to enhance service-to-service communication with more control, security, and visibility.

## How to store credentials in k8s?

Consider using external tools like HashiCorp Vault, AWS Secrets Manager, or Azure Key Vault for more advanced secrets management capabilities, including encryption, audit trails, and fine-grained access controls.

Other options:
- My experience: Encrypt credentials and then store in config maps.
- It is well known that secrets encoded in base64 within etcd are not safe! The reason why there is a native Kubernetes pattern addressing this specific need within the Secret Management framework - the Kubernetes KMS provider plugin for data encryption.
- Use a secrets provider like HashiCorp Vault or if you are on the cloud, they most likely have a secrets solution available (e.g. Secrets Manager in AWS). For Vault, you can either inject the secrets with a sidecar or use the Secrets store CSI driver which is also implemented by various cloud providers.
- References:
  - https://www.ondat.io/blog/how-to-keep-a-secret-secret-within-kubernetes
  - https://www.reddit.com/r/kubernetes/comments/rl01ms/i_have_sensitive_information_in_my_configmap_how/

## Q&A

- Does Kubernetes use Docker?
  - Kubernetes can be used with or without Docker.
  - Kubernetes is deprecating Docker as a container runtime after v1.20. Docker as an underlying runtime is being deprecated in favor of runtimes that use the Container Runtime Interface (CRI) created for Kubernetes. Docker-produced images will continue to work in your cluster with all runtimes, as they always have.
  - Ref: https://kubernetes.io/blog/2020/12/02/dont-panic-kubernetes-and-docker/
- What is the main difference between Docker and Kubernetes?
  - A fundamental difference between Kubernetes and Docker is that Kubernetes is meant to run across a cluster while Docker runs on a single node.
- What is a Kubernetes pod?
  - Pods are the smallest deployable units created and managed in Kubernetes.
  - A good practice is to run only one container inside a single pod. If you would like to scale up your microservice you would just have to increase a number of running pods. 
  - All running pods that belong to a single microservice are logically grouped by Kubernetes Service. This service may be visible outside the cluster, and is able to load balance incoming requests between all running pods. 
  - By putting multiple services into a single pod, you’re reducing scalability and HA. You lose the ability to scale independently - there is always a fixed relationship between the containers in the pod. It also means very tight coupling between your services, which can lead to bad design in the future, unless you are careful. 
    - If you’re throwing every container into one pod, you’re not using K8s as it’s designed. K8s high-availability is done at a pod level: liveness/readiness probes, service endpoint registration, rolling updates, and more. If any process in that pod fails, is updated, or is restarted, the whole pod must be destroyed, recreated, checked and registered with the service.
  - Ref: https://www.reddit.com/r/kubernetes/comments/12cdxm1/is_it_a_bad_idea_to_run_a_pod_that_contain/
- What is Ingress vs ClusterIP vs NodePort vs LoadBalancer?
  - A ClusterIP service is the default Kubernetes service. It gives you a service inside your cluster that other apps inside your cluster can access. There is no external access.
  - A NodePort service is the most primitive way to get external traffic directly to your service. NodePort, as the name implies, opens a specific port on all the Nodes (the VMs), and any traffic that is sent to this port is forwarded to the service.
    - You can only have one service per port
    - You can only use ports 30000–32767
    - If your Node/VM IP address change, you need to deal with that
  - A LoadBalancer service is the standard way to expose a service to the internet. On GKE, this will spin up a Network Load Balancer that will give you a single IP address that will forward all traffic to your service.
    - The big downside is that each service you expose with a LoadBalancer will get its own IP address, and you have to pay for a LoadBalancer per exposed service, which can get expensive!
  - Ingress is the most useful if you want to expose multiple services under the same IP address, and these services all use the same L7 protocol (typically HTTP). Ingress is actually NOT a type of service. Instead, it sits in front of multiple services and act as a `smart router` or entrypoint into your cluster.
  - Ref: https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0
- What is ReplicaSet vs Deployment?
  - A ReplicaSet ensures that a specified number of pod replicas are running at any given time. However, a Deployment is a higher-level concept that manages ReplicaSets and provides declarative updates to Pods along with a lot of other useful features. It is a self-healing mechanism.
  - Deployment resource makes it easier for updating your pods to a newer version. 
    - Let's say you use ReplicaSet-A for controlling your pods, then You wish to update your pods to a newer version, now you should create Replicaset-B, scale down ReplicaSet-A and scale up ReplicaSet-B by one step repeatedly (This process is known as rolling update). Although this does the job, but it's not a good practice and it's better to let K8S do the job. 
    - A Deployment resource does this automatically without any human interaction and increases the abstraction by one level. 
    - Note: Deployment doesn't interact with pods directly, it just does rolling update using ReplicaSets.
  - Ref: https://stackoverflow.com/questions/69448131/kubernetes-whats-the-difference-between-deployment-and-replica-set
- What's the difference between a Service and a Deployment in Kubernetes?
  - A deployment is responsible for keeping a set of pods running.
  - A service is responsible for enabling network access to a set of pods. We could use a deployment without a service to keep a set of identical pods running in the Kubernetes cluster.
  - Ref: https://matthewpalmer.net/kubernetes-app-developer/articles/service-kubernetes-example-tutorial.html
- What's ingress?
  - In Kubernetes, an Ingress is an object that allows access to your Kubernetes services from outside the Kubernetes cluster.
  - You configure access by creating a collection of rules that define which inbound connections reach which services.
  - Ref: https://matthewpalmer.net/kubernetes-app-developer/articles/kubernetes-ingress-guide-nginx-example.html
- What is KMS? How is it used with K8?
  - KMS provides an interface for a provider to utilize a key stored in an external key service to perform this encryption. Encryption at rest using KMS v1 has been a feature of Kubernetes since version v1. 10, and is currently in beta as of version v1.
  - The KMS provider uses envelope encryption: Kubernetes encrypts resources using a data key, and then encrypts that data key using the managed encryption service. Kubernetes generates a unique data key for each resource.
  - KMS providers: AWS KMS, Google CloudKMS...
  - References
    - https://kubernetes.io/docs/tasks/administer-cluster/kms-provider/
    - https://kubernetes.io/blog/2022/09/09/kms-v2-improvements/#:~:text=KMS%20provides%20an%20interface%20for,12
- What is the difference between Kubernetes ConfigMaps and Secrets?
  - ConfigMaps are typically used for non-sensitive configuration data, while Secrets are used for storing sensitive information (but they are not encrypted! only encoded with base64). ConfigMaps stores data as key-value pairs, whereas Secrets stores data as base64-encoded data.
  - Both ConfigMaps and secrets store the data the same way, with key/value pairs.
  - You can use RBAC to give access to configmaps and not to secrets to specific service accounts / users. RBAC is only for API access, not volumes.
  - When a ConfigMap is updated, Kubernetes does not automatically propagate these changes to Pods. However, you can design your application to watch for changes in the ConfigMap and respond accordingly.
  - If you update the secret, the change will propagate to all the pods that reference it. [Reference](https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets)
  - References:
    - https://kubernetes.io/docs/concepts/configuration/secret/
- What are types of volume?
  - configMap
  - Ref: https://kubernetes.io/docs/concepts/storage/volumes/
