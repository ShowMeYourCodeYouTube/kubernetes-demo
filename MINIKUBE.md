# minikube

Minikube is a lightweight Kubernetes implementation that creates a VM on your local machine and deploys a simple cluster containing only one node. Minikube is available for Linux, macOS, and Windows systems. The Minikube CLI provides basic bootstrapping operations for working with your cluster, including start, stop, status, and delete.

It's recommended to use `Chocolatey` on Windows and `Homebrew` on macOS in order to install `minikube` for local
development.

Ref: https://minikube.sigs.k8s.io/docs/start/

Commands:

- minikube start --driver=docker --kubernetes-version v1.21.0
- minikube dashboard
- minikube config set driver docker / minikube start --driver=docker
